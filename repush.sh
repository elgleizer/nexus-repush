

src_image = docker-compose-example
src_path = elgleizer
version = latest

docker pull registry.gitlab.com/${src_path}/${src_image}:${version}
docker tag registry.gitlab.com/${src_path}/${src_image}:${version} repush-example.nexus.region.vtb.ru/${src_image}:${version}
docker push repush-example.nexus.region.vtb.ru/${src_image}:${version}
